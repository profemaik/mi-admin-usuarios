FROM ubuntu:22.04

LABEL maintainer="Maikel Carballo"

ARG WWWGROUP
ARG NODE_VERSION=16
ARG VERSION_CODENAME=jammy

WORKDIR /var/www/html

ENV DEBIAN_FRONTEND noninteractive
ENV TZ=America/Caracas

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update; \
    apt-get upgrade -y; \
    apt-get install -y \
        gnupg \
        gosu \
        curl \
        ca-certificates \
        zip \
        unzip \
        git \
        locales \
        supervisor \
        sqlite3 \
        libcap2-bin \
        libpng-dev \
        python2 \
    ; \
    \
    mkdir -p ~/.gnupg; \
    chmod 600 ~/.gnupg; \
    echo "disable-ipv6" >> ~/.gnupg/dirmngr.conf; \
    apt-key adv --homedir ~/.gnupg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys E5267A6C; \
    apt-key adv --homedir ~/.gnupg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C300EE8C; \
    \
    echo "deb http://ppa.launchpad.net/ondrej/php/ubuntu $VERSION_CODENAME main" > /etc/apt/sources.list.d/ppa_ondrej_php.list; \
    apt-get update; \
    apt-get upgrade -y; \
    apt-get install -y \
        php7.4-cli \
        php7.4-dev \
        php7.4-pgsql \
        php7.4-mysql \
        php7.4-sqlite3 \
        php7.4-ldap \
        php7.4-memcached \
        php7.4-redis \
        php7.4-imap \
        php7.4-curl \
        php7.4-readline \
        php7.4-intl \
        php7.4-mbstring \
        php7.4-xml \
        php7.4-zip \
        php7.4-bcmath \
        php7.4-soap \
        php7.4-igbinary \
        php7.4-gd \
        php7.4-pcov \
        php7.4-msgpack \
        php7.4-xdebug \
    ; \
    \
    php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer; \
    \
    curl -sL https://deb.nodesource.com/setup_$NODE_VERSION.x | bash -; \
    apt-get install -y \
        nodejs; \
    \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -; \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list; \
    apt-get update; \
    apt-get upgrade -y; \
    apt-get install -y \
        yarn; \
    \
    echo "deb http://apt.postgresql.org/pub/repos/apt $VERSION_CODENAME-pgdg main" > /etc/apt/sources.list.d/pgdg.list; \
    curl https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -; \
    apt-get update; \
    apt-get upgrade -y; \
    apt-get install -y \
        mysql-client \
        postgresql-client \
    ; \
    \
    apt-get -y autoremove; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN localedef -i es_VE -c -f UTF-8 -A /usr/share/locale/locale.alias es_VE.UTF-8

ENV LANG es_VE.utf8

RUN setcap "cap_net_bind_service=+ep" /usr/bin/php7.4

RUN groupadd --force -g $WWWGROUP sail
RUN useradd -ms /bin/bash --no-user-group -g $WWWGROUP -u 1337 sail

COPY start-container /usr/local/bin/start-container
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY php.ini /etc/php/7.4/cli/conf.d/99-sail.ini
RUN chmod +x /usr/local/bin/start-container

EXPOSE 8000

ENTRYPOINT ["start-container"]
