import { DateTime } from 'luxon'
import { ref } from 'vue'

export const useDTlocale = (dt) => {
  const dtLocale = ref(null)

  if (!dt) {
    return { dtLocale }
  }

  dtLocale.value = DateTime
                    .fromISO(dt)
                    .toLocaleString(DateTime.DATETIME_MED, { locale: 'es-ve' })

  return { dtLocale }
}
