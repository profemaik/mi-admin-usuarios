import { ref } from 'vue'

export const usePaginationData = (data) => {
  const currentPage = ref(null)
  const itemsPerPage = ref(null)
  const links = ref(null)
  const nextPage = ref(null)
  const prevPage = ref(null)
  const totalPages = ref(null)

  if (data) {
    currentPage.value = data.current_page
    itemsPerPage.value = data.per_page
    links.value = data.links
    nextPage.value = data.next_page_url
    prevPage.value = data.prev_page_url
    totalPages.value = data.last_page
  }

  return {
    currentPage,
    itemsPerPage,
    links,
    nextPage,
    prevPage,
    totalPages,
  }
}