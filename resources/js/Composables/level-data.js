import { ref } from 'vue'

export const useLevelPropsCreate = (page) => {
  const lvl1crumb = ref(null)
  const lvl1icon = ref(null)
  const lvl1route = ref(null)
  const lvl2crumb = ref('Nuevo')
  const lvl2icon = ref('fas fa-plus')

  switch (page) {
    case 'Genders':
      lvl1crumb.value = 'Géneros Sociales'
      lvl1icon.value = 'fas fa-venus-mars'
      lvl1route.value = route(`${page.toLowerCase()}.index`)
      break;

    case 'Marital_Statuses':
      lvl1crumb.value = 'Estados Civiles'
      lvl1icon.value = 'fas fa-heart'
      lvl1route.value = route(`${page.toLowerCase()}.index`)
      break;

    case 'Roles':
      lvl1crumb.value = 'Roles'
      lvl1icon.value = 'fas fa-user-tag'
      lvl1route.value = route(`${page.toLowerCase()}.index`)
      break;

    case 'Users':
      lvl1crumb.value = 'Usuarios'
      lvl1icon.value = 'fas fa-users'
      lvl1route.value = route(`${page.toLowerCase()}.index`)
      lvl2icon.value = 'fas fa-user-plus'
      break;

    default:
      break;
  }

  return { lvl1crumb, lvl1icon, lvl1route, lvl2crumb, lvl2icon  }
}

export const useLevelPropsEdit = (page) => {
  const lvl1crumb = ref(null)
  const lvl1icon = ref(null)
  const lvl1route = ref(null)
  const lvl2crumb = ref('Modificar')
  const lvl2icon = ref('fas fa-pen')

  switch (page) {
    case 'Genders':
      lvl1crumb.value = 'Géneros Sociales'
      lvl1icon.value = 'fas fa-venus-mars'
      lvl1route.value = route(`${page.toLowerCase()}.index`)
      break;

    case 'Marital_Statuses':
      lvl1crumb.value = 'Estados Civiles'
      lvl1icon.value = 'fas fa-heart'
      lvl1route.value = route(`${page.toLowerCase()}.index`)
      break;

    case 'Roles':
      lvl1crumb.value = 'Roles'
      lvl1icon.value = 'fas fa-user-tag'
      lvl1route.value = route(`${page.toLowerCase()}.index`)
      break;

    case 'Users':
      lvl1crumb.value = 'Usuarios'
      lvl1icon.value = 'fas fa-users'
      lvl1route.value = route(`${page.toLowerCase()}.index`)
      lvl2icon.value = 'fas fa-user-edit'
      break;

    default:
      break;
  }

  return { lvl1crumb, lvl1icon, lvl1route, lvl2crumb, lvl2icon  }
}

export const useLevelPropsIndex = (page) => {
  const lvl1crumb = ref(null)
  const lvl1icon = ref(null)

  switch (page) {
    case 'Activities':
      lvl1crumb.value = 'Actividades'
      lvl1icon.value = 'fas fa-file-medical-alt'
      break;

    case 'Genders':
      lvl1crumb.value = 'Géneros Sociales'
      lvl1icon.value = 'fas fa-venus-mars'
      break;

    case 'Marital_Statuses':
      lvl1crumb.value = 'Estados Civiles'
      lvl1icon.value = 'fas fa-heart'
      break;

    case 'Permissions':
      lvl1crumb.value = 'Permisos'
      lvl1icon.value = 'fas fa-lock'
      break;

    case 'Roles':
      lvl1crumb.value = 'Roles'
      lvl1icon.value = 'fas fa-user-tag'
      break;

    case 'Users':
      lvl1crumb.value = 'Usuarios'
      lvl1icon.value = 'fas fa-users'
      break;

    default:
      break;
  }

  return { lvl1crumb, lvl1icon  }
}

export const useLevelPropsSearch = (data) => {
  const from = ref(null)
  const to = ref(null)
  const total = ref(null)
  const subject = ref(null)

  if (data) {
    from.value = data.from
    to.value = data.to
    total.value = data.total
    subject.value = data.total !== 1 ? 'registros' : 'registro'
  }

  return { from, to, total, subject }
}
