import { ref } from 'vue'

export const useUserAgentData = (userAgent) => {
  const browserIcon = ref('fas fa-globe')
  const browserVersion = ref(null)
  const platformIcon = ref('fas fa-computer')

  switch (userAgent.browser) {
    case 'Chrome':
      browserIcon.value = 'fab fa-chrome fa-fw'
      break

    case 'Firefox':
      browserIcon.value = 'fab fa-firefox-browser'
      break

    case 'Opera':
      browserIcon.value = 'fab fa-opera'
      break

    case 'Safari':
      browserIcon.value = 'fab fa-safari'
      break

    case 'Internet Explorer':
      if (userAgent.version < 38) {
        browserIcon.value = 'fab fa-internet-explorer'
      }
      if (userAgent.version >= 38 && userAgent.version < 74) {
        browserIcon.value = 'fab fa-edge-legacy'
      }
      if (userAgent.version > 74) {
        browserIcon.value = 'fab fa-edge'
      }
      break

    default:
      break
  }

  switch (userAgent.platform) {
    case 'Windows':
      platformIcon.value = 'fab fa-windows'
      break

    case 'iPad':
      platformIcon.value = 'fas fa-tablet-alt'
      break

    case 'iPhone':
      platformIcon.value = 'fas fa-mobile'
      break

    case 'Macintosh':
      platformIcon.value = 'fab fa-apple'
      break

    case 'Linux; Android':
      platformIcon.value = 'fab fa-android'
      break

    case 'BlackBerry':
      platformIcon.value = 'fab fa-blackberry'
      break

    case 'Linux':
      platformIcon.value = 'fab fa-linux fa-fw'
      break

    case 'Ubuntu':
      platformIcon.value = 'fab fa-ubuntu'
      break

    case 'CrOS':
      platformIcon.value = 'fab fa-chrome'
      break

    default:
      break
  }

  browserVersion.value = userAgent.version

  return { browserIcon, browserVersion, platformIcon }
}