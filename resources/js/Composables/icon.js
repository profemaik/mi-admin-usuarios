import { ref } from 'vue'

export const useIconSize = ((value) => {
  const iconSize = ref('')

  if (value === 'is-small') {
    iconSize.value = 'is-small'
  }

  if (value === 'is-medium') {
    iconSize.value = ''
  }

  if (value === 'is-large') {
    iconSize.value = 'is-medium'
  }

  return { iconSize }
})