import axios from 'axios'
import { ref, isRef, unref, watchEffect } from 'vue'

export const useMunicipalities = (idState) => {
  const data = ref(null)
  const error = ref(false)
  const loading = ref(true)

  function getMunicipalities() {
    data.value = null
    error.value = false
    loading.value = true

    axios
      .get(`${route('ve.municipalities')}?state_id=${unref(idState)}`)
      .then(res => { data.value = res.data })
      .catch(err => {
        console.log(err)
        error.value = true
      })
      .finally(() => loading.value = false)
  }

  if (isRef(idState)) {
    watchEffect(getMunicipalities)
  } else {
    getMunicipalities()
  }

  return { data, error, loading }
}

export const useParishes = (idMunicipality) => {
  const data = ref(null)
  const error = ref(false)
  const loading = ref(true)

  function getParishes() {
    data.value = null
    error.value = false
    loading.value = true

    axios
      .get(`${route('ve.parishes')}?municipality_id=${unref(idMunicipality)}`)
      .then(res => { data.value = res.data })
      .catch(err => {
        console.log(err)
        error.value = true
      })
    .finally(() => loading.value = false)
  }

  if (isRef(idMunicipality)) {
    watchEffect(getParishes)
  } else {
    getParishes()
  }

  return { data, error, loading }
}

export const useCities = (idState) => {
  const data = ref(null)
  const error = ref(false)
  const loading = ref(true)

  function getCities() {
    data.value = null
    error.value = false
    loading.value = true

    axios
      .get(`${route('ve.cities')}?state_id=${unref(idState)}`)
      .then(res => { data.value = res.data })
      .catch(err => {
        console.log(err)
        error.value = true
      })
      .finally(() => loading.value = false)
  }

  if (isRef(idState)) {
    watchEffect(getCities)
  } else {
    getCities()
  }

  return { data, error, loading }
}