module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es2021": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:vue/vue3-recommended"
    ],
    "parserOptions": {
        "ecmaVersion": 13
    },
    "plugins": [
        "vue"
    ],
    "rules": {
        "indent": ['error', 2],
        'quotes': ['warn', 'single'],
        'semi': ['warn', 'never'],
        'comma-dangle': ['warn', 'always-multiline'],
        'vue/max-attributes-per-line': false,
        'vue/require-default-prop': false,
        'vue/singleline-html-element-content-newline': false,
    }
};
