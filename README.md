# Mi Admin Usuarios

Administrador de Usuarios

[![Captura de pantalla Mi Admin Usuarios][product-screenshot]](https://gitlab.com/profemaik/php-codeigniter-scbmk)

## Tabla de Contenidos

1. [Construido con](#construido-con)
2. [Primeros Pasos](#primeros-pasos)
3. [Uso](#uso)
4. [Hoja de Ruta](#hoja-de-ruta)
5. [Contribuir](#contribuir)
6. [Licencia](#licencia)
7. [Contacto](#contacto)
8. [Agradecimientos](#agradecimientos)

## Construido con

* Back-end
  * [PHP 8.1](https://www.php.net)
  * [Laravel 8](https://laravel.com)
  * [Laravel Breeze](https://laravel.com/docs/8.x/starter-kits#laravel-breeze)
  * [PostgreSQL 14](https://www.postgresql.org)
* Front-end
  * [Inertia](https://inertiajs.com)
  * [Vue 3](https://v3.vuejs.org)
  * [Bulma](https://bulma.io)

## Primeros Pasos

### Prerequisitos

* [Docker con Docker Compose](https://docs.docker.com/get-docker/).

### Instalación rápida

1. Clone el repositorio

   ```sh
   git clone https://gitlab.com/profemaik/mi-admin-usuarios.git
   ```

2. Cree el archivo `.env`

   ```sh
   cp .env.example .env
   ```

3. Defina la base de datos y otros ajustes en el archivo `.env` anteriormente creado.

4. Instale las dependencias backend

   ```sh
   docker run --rm \
        -u "$(id -u):$(id -g)" \
        -v $(pwd):/var/www/html \
        -w /var/www/html \
        laravelsail/php81-composer:latest \
        composer install --ignore-platform-reqs
   ```

5. Inicie los servicios necesarios para ejecutar el sistema

   ```sh
   sail up -d

   # Si no tiene configurado el alias para Sail de Laravel
   ./vendor/bin/sail up -d
   ```

6. Abra un navegador web y diríjase a la dirección `http://localhost`.

7. Inicie sesión con las credenciales:
   * usuario: `admin@localhost`
   * contraseña: `Admin123.`

Ya está listo para usar el sistema.

## Uso

_En construcción._

## Hoja de Ruta

Eche un vistazo a las [incidencias](https://gitlab.com/profemaik/mi-admin-usuarios/-/issues) para que conozca nuevas funcionalidades propuestas y/o problemas conocidos. Siéntase libre de
solicitar nuevas funcionalidades o aportar nuevas ideas.

## Contribuir

Las contribuciones son las que hacen que la comunidad de código abierto sea un lugar tan increíble para aprender, inspirar y crear. Cualquier contribución que haga es **muy apreciada**.

1. Realice un fork del proyecto
2. Cree su rama feature (`git checkout -b feature/NuevaFuncionalidad`)
3. Confirme sus cambios (`git commit -m 'Agrega nueva funcionalidad'`)
4. Ejecute un Push a la rama (`git push origin feature/NuevaFuncionalidad`)
5. Abra una petición de fusión (Merge)

## Licencia

Se distribuye bajo la Licencia MIT.

## Contacto

[Maikel Carballo](https:twitter.com/_kimael_)

Enlace del Proyecto: [SCBmk](https://gitlab.com/profemaik/php-codeigniter-scbmk)

## Agradecimientos

* [Bulma Admin Dashboard Template](https://github.com/mazipan/bulma-admin-dashboard-template)
* [Luxon](https://moment.github.io/luxon/#/)

[product-screenshot]: resources/imgs/repo/mi-admin-usuarios-screenshot.jpg
