<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActivityLogConfSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('activity_log_confs')->insert([
            [
                'about'      => trans('Default'),
                'created_at' => 'now()',
                'updated_at' => 'now()',
            ],
        ]);
    }
}
