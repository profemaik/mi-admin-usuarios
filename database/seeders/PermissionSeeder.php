<?php

namespace Database\Seeders;

use App\Helpers\PermissionHelper;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $routenames = PermissionHelper::permissibleRoutes();

        foreach ($routenames as $route) {
            DB::table('permissions')->insert([
                'route_name' => $route,
                'about'      => __("permissions.$route"),
                'created_at' => 'now()',
                'updated_at' => 'now()',
            ]);
        }
    }
}
