<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genders')->insert([
            [
                'name'    => 'Femenino',
                'fa_icon' => 'fas fa-venus',
                'about'   => 'Que se identifica como mujer.',
                'created_at' => 'now()',
                'updated_at' => 'now()',
            ],
            [
                'name'    => 'Masculino',
                'fa_icon' => 'fas fa-mars',
                'about'   => 'Que se identifica como hombre.',
                'created_at' => 'now()',
                'updated_at' => 'now()',
            ],
            [
                'name'    => 'No Binario',
                'fa_icon' => 'fas fa-mercury',
                'about'   => 'Que no se identifica completamente ni como mujer ni como hombre.',
                'created_at' => 'now()',
                'updated_at' => 'now()',
            ],
            [
                'name'    => 'Transgénero',
                'fa_icon' => 'fas fa-transgender-alt',
                'about'   => 'Que tienen una identidad o expresión de género que difiere del sexo que se le asignó al nacer.',
                'created_at' => 'now()',
                'updated_at' => 'now()',
            ],
            [
                'name'    => 'Neutral',
                'fa_icon' => 'fas fa-neuter',
                'about'   => 'Que evita distinguir (o especificar) roles según el sexo o género de las personas.',
                'created_at' => 'now()',
                'updated_at' => 'now()',
            ],
            [
                'name'    => 'Prefiero no decirlo',
                'fa_icon' => 'fas fa-genderless',
                'about'   => 'Que desea mantener en privado su género.',
                'created_at' => 'now()',
                'updated_at' => 'now()',
            ],
        ]);
    }
}
