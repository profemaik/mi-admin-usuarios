<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email'             => 'admin@localhost',
            'email_verified_at' => 'now()',
            'password'          => password_hash('Admin123.', PASSWORD_DEFAULT),
            'remember_token'    => Str::random(10),
            'avatar'            => asset('imgs/users_default/undraw_profile.svg'),
            'is_admin'          => true,
            'created_at'        => 'now()',
            'updated_at'        => 'now()',
        ]);
    }
}
