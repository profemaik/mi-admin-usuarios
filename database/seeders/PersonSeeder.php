<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('people')->insert([
            [
                'user_id'     => 1,
                'nationality' => 'V',
                'id_national' => '-',
                'names'       => '-',
                'surnames'    => '-',
            ],
        ]);
    }
}
