<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VeStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $query = "INSERT INTO ve_states (id, name, iso_3166_2, created_at, updated_at)
                VALUES (1, 'Amazonas', 'VE-X', 'now()', 'now()'),
                       (2, 'Anzoátegui', 'VE-B', 'now()', 'now()'),
                       (3, 'Apure', 'VE-C', 'now()', 'now()'),
                       (4, 'Aragua', 'VE-D', 'now()', 'now()'),
                       (5, 'Barinas', 'VE-E', 'now()', 'now()'),
                       (6, 'Bolívar', 'VE-F', 'now()', 'now()'),
                       (7, 'Carabobo', 'VE-G', 'now()', 'now()'),
                       (8, 'Cojedes', 'VE-H', 'now()', 'now()'),
                       (9, 'Delta Amacuro', 'VE-Y', 'now()', 'now()'),
                       (10, 'Falcón', 'VE-I', 'now()', 'now()'),
                       (11, 'Guárico', 'VE-J', 'now()', 'now()'),
                       (12, 'Lara', 'VE-K', 'now()', 'now()'),
                       (13, 'Mérida', 'VE-L', 'now()', 'now()'),
                       (14, 'Miranda', 'VE-M', 'now()', 'now()'),
                       (15, 'Monagas', 'VE-N', 'now()', 'now()'),
                       (16, 'Nueva Esparta', 'VE-O', 'now()', 'now()'),
                       (17, 'Portuguesa', 'VE-P', 'now()', 'now()'),
                       (18, 'Sucre', 'VE-R', 'now()', 'now()'),
                       (19, 'Táchira', 'VE-S', 'now()', 'now()'),
                       (20, 'Trujillo', 'VE-T', 'now()', 'now()'),
                       (21, 'La Guaira', 'VE-W', 'now()', 'now()'),
                       (22, 'Yaracuy', 'VE-U', 'now()', 'now()'),
                       (23, 'Zulia', 'VE-V', 'now()', 'now()'),
                       (24, 'Distrito Capital', 'VE-A', 'now()', 'now()'),
                       (25, 'Dependencias Federales', 'VE-Z', 'now()', 'now()')";
        DB::unprepared($query);
    }
}
