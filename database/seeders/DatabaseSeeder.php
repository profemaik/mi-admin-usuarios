<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            GenderSeeder::class,
            MaritalStatusSeeder::class,
            RoleSeeder::class,
            PermissionSeeder::class,
            VeStateSeeder::class,
            VeMunicipalitySeeder::class,
            VeParishSeeder::class,
            VeCitySeeder::class,
            UserSeeder::class,
            RoleUserSeeder::class,
            PersonSeeder::class,
            ActivityLogConfSeeder::class,
            OrganizationSeeder::class,
        ]);
    }
}
