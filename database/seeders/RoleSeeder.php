<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name'       => 'Administrador',
                'about'      => 'Usuario con capacidad de gestionar partes '
                              . 'esenciales del sistema.',
                'created_at' => 'now()',
                'updated_at' => 'now()',
            ],
            [
                'name'       => 'Usuario Final',
                'about'      => 'Persona que usa el sistema.',
                'created_at' => 'now()',
                'updated_at' => 'now()',
            ],
        ]);
    }
}
