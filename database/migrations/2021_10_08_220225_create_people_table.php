<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->id()->generatedAs()->always();
            $table->foreignId('user_id')
                  ->nullable()
                  ->constrained()
                  ->cascadeOnDelete();
            $table->foreignId('marital_status_id')
                  ->nullable()
                  ->constrained()
                  ->cascadeOnDelete();
            $table->foreignId('gender_id')
                  ->nullable()
                  ->constrained()
                  ->cascadeOnDelete();
            $table->foreignId('ve_parish_id')
                  ->nullable()
                  ->constrained()
                  ->cascadeOnDelete();
            $table->foreignId('ve_city_id')
                  ->nullable()
                  ->constrained()
                  ->cascadeOnDelete();
            $table->string('nationality', 1)->default('V');
            $table->string('id_national', 11)->unique();
            $table->string('names', 256);
            $table->string('surnames', 256);
            $table->date('birth_date')->nullable();
            $table->string('address', 512)->nullable();
            $table->string('postal_code', 8)->nullable();
            $table->string('phones')->nullable();
            $table->string('emails')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
