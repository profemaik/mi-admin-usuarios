<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityLogConfsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_log_confs', function (Blueprint $table) {
            $table->id()->generatedAs()->always();
            $table->string('about');
            $table->boolean('auth_events_all')->default(false);     # Puede causar exceso de redundancia
            $table->boolean('auth_attempts')->default(false);       # Puede causar exceso de redundancia
            $table->boolean('auth_attempts_failed')->default(true); # Inicios de sesión fallidos
            $table->boolean('lock_out')->default(true);           # Usuario bloqueado
            $table->boolean('password_reset')->default(true);
            $table->boolean('login_success')->default(true);
            $table->boolean('logout_success')->default(true);
            $table->boolean('logout_current_device')->default(true);
            $table->boolean('logout_other_device')->default(true);
            $table->boolean('user_registered')->default(true);
            $table->boolean('user_validated')->default(true);
            $table->boolean('user_verified')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_log_confs');
    }
}
