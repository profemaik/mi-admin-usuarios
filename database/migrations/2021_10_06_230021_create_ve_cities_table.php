<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVeCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ve_cities', function (Blueprint $table) {
            $table->id()->generatedAs();
            $table->foreignId('ve_state_id')
                  ->constrained()
                  ->cascadeOnDelete();
            $table->string('name', 256);
            $table->boolean('is_capital')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ve_cities');
    }
}
