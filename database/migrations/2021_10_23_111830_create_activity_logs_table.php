<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_logs', function (Blueprint $table) {
            $table->id()->generatedAs()->always();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('user_name');
            $table->text('action');
            $table->text('http_route');
            $table->ipAddress('ip_address');
            $table->string('user_agent');
            $table->string('locale');
            $table->string('referer')->nullable();
            $table->string('method_type');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_logs');
    }
}
