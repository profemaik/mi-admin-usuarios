<?php

use App\Http\Controllers\VeGeoPoliticalInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/ve-municipalities', [
    VeGeoPoliticalInfo::class, 'getVeMunicipalities'
])->name('ve.municipalities');

Route::get('/ve-parishes', [
    VeGeoPoliticalInfo::class, 'getVeParishes'
])->name('ve.parishes');

Route::get('/ve-cities', [
    VeGeoPoliticalInfo::class, 'getVeCities'
])->name('ve.cities');
