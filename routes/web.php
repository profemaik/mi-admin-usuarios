<?php

use App\Http\Controllers\ActivityLogController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GenderController;
use App\Http\Controllers\MaritalStatusController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => false,
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
        'bulmaBadge' => asset('imgs/badges/made-with-bulma--dark.png'),
    ]);
});

// TODO añadir y crear el middleware para verificar el rol de admin
Route::middleware(['auth', 'verified'])->group(function ()
{
    Route::get('/dashboard', [DashboardController::class, 'index'])
         ->name('dashboard');
    Route::get('/activities', [ActivityLogController::class, 'index'])
         ->name('activities');

    Route::get('/permissions', [PermissionController::class, 'index'])
         ->name('permissions.index');
    Route::get('/permissions/{permission}', [PermissionController::class, 'show'])
         ->name('permissions.show');
    Route::post('/permissions', [PermissionController::class, 'refresh'])
         ->name('permissions.refresh');

    Route::resources([
        'genders'          => GenderController::class,
        'users'            => UserController::class,
        'roles'            => RoleController::class,
        'marital_statuses' => MaritalStatusController::class,
    ]);
});

require __DIR__.'/auth.php';
