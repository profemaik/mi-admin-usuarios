<?php

namespace App\Listeners;

use App\Models\ActivityLogConf;
use App\Traits\ActivityLogger;
use Illuminate\Auth\Events\Failed;

class LogFailedLogin
{
    use ActivityLogger;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Failed  $event
     * @return void
     */
    public function handle(Failed $event)
    {
        $isLoggable = ActivityLogConf::first()->auth_attempts_failed;

        if ($isLoggable)
        {
            $email = $event->credentials['email'];

            $userId = $event->user ? $event->user->id : null;
            $userName = $event->user ? $event->user->email : $email;

            $action = trans('falló en iniciar sesión');

            $data = [
                'user_id'   => $userId,
                'user_name' => $userName,
                'action'    => $action,
            ];

            $this->logActivity($data);
        }
    }
}
