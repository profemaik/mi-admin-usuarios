<?php

namespace App\Listeners;

use App\Models\ActivityLogConf;
use App\Traits\ActivityLogger;
use Illuminate\Auth\Events\Validated;

class LogValidated
{
    use ActivityLogger;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Validated  $event
     * @return void
     */
    public function handle(Validated $event)
    {
        $isLoggable = ActivityLogConf::first()->user_validated;

        if ($isLoggable)
        {
            $userId = $event->user->id;
            $username = $event->user->email;
            $action = trans('usuario ha sido validado');

            $data = [
                'user_id'   => $userId,
                'user_name' => $username,
                'action'    => $action,
            ];

            $this->logActivity($data);
        }
    }
}
