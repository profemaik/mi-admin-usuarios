<?php

namespace App\Listeners;

use App\Models\ActivityLogConf;
use App\Traits\ActivityLogger;
use Illuminate\Auth\Events\Authenticated;

class LogAuthenticated
{
    use ActivityLogger;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Authenticated  $event
     * @return void
     */
    public function handle(Authenticated $event)
    {
        $isLoggable = ActivityLogConf::first()->auth_events_all;

        if ($isLoggable)
        {
            $data = [
                'user_id'   => $event->user->id,
                'user_name' => $event->user->email,
                'action'    => trans('usuario autenticado'),
            ];

            $this->logActivity($data);
        }
    }
}
