<?php

namespace App\Listeners;

use App\Models\ActivityLogConf;
use App\Models\User;
use App\Traits\ActivityLogger;
use Illuminate\Auth\Events\Attempting;

class LogAuthenticationAttempt
{
    use ActivityLogger;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Attempting  $event
     * @return void
     */
    public function handle(Attempting $event)
    {
        $isLoggable = ActivityLogConf::first()->auth_attempts;

        if ($isLoggable)
        {
            $email = $event->credentials['email'];

            $user = User::firstWhere('email', '=', $email);
            $userId = $user ? $user->id : null;
            $userName = $user ? $user->email : $email;

            $action = trans('intentó iniciar sesión');

            $data = [
                'user_id'   => $userId,
                'user_name' => $userName,
                'action'    => $action,
            ];

            $this->logActivity($data);
        }
    }
}
