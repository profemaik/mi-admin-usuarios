<?php

namespace App\Listeners;

use App\Models\ActivityLogConf;
use App\Traits\ActivityLogger;
use Illuminate\Auth\Events\Verified;

class LogVerified
{
    use ActivityLogger;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Verified  $event
     * @return void
     */
    public function handle(Verified $event)
    {
        $isLoggable = ActivityLogConf::first()->user_verified;

        if ($isLoggable)
        {
            $userId = $event->user->id;
            $userName = $event->user->email;
            $action = trans('usuario ha sido verificado');

            $data = [
                'user_id'   => $userId,
                'user_name' => $userName,
                'action'    => $action,
            ];

            $this->logActivity($data);
        }
    }
}
