<?php

namespace App\Listeners;

use App\Models\ActivityLog;
use App\Models\ActivityLogConf;
use App\Models\User;
use App\Traits\ActivityLogger;
use Illuminate\Auth\Events\Lockout;

class LogLockout
{
    use ActivityLogger;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Lockout  $event
     * @return void
     */
    public function handle(Lockout $event)
    {
        $isLoggable = ActivityLogConf::first()->lock_out;

        if ($isLoggable)
        {
            $email = $event->request->request->get('email');

            $user = User::firstWhere('email', '=', $email);
            $userId = $user ? $user->id : null;
            $userName = $user ? $user->email : $email;

            $action = trans('ha sido bloqueado');

            $data = [
                'user_id'     => $userId,
                'user_name'   => $userName,
                'action'      => $action,
                'http_route'  => $event->request->fullUrl(),
                'ip_address'  => $event->request->ip(),
                'user_agent'  => $event->request->header('user-agent'),
                'locale'      => $event->request->header('accept-language'),
                'referer'     => $event->request->header('referer'),
                'method_type' => $event->request->method(),
            ];

            ActivityLog::create($data);
        }
    }
}
