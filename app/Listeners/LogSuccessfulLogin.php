<?php

namespace App\Listeners;

use App\Models\ActivityLogConf;
use App\Traits\ActivityLogger;
use Illuminate\Auth\Events\Login;

class LogSuccessfulLogin
{
    use ActivityLogger;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $isLoggable = ActivityLogConf::first()->login_success;

        if ($isLoggable)
        {
            $userId = $event->user->id;
            $userName = $event->user->email;
            $action = trans('inició sesión');

            $data = [
                'user_id'   => $userId,
                'user_name' => $userName,
                'action'    => $action,
            ];

            $this->logActivity($data);
        }
    }
}
