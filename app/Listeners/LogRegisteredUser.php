<?php

namespace App\Listeners;

use App\Models\ActivityLog;
use App\Models\ActivityLogConf;
use App\Traits\ActivityLogger;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Request;

class LogRegisteredUser
{
    use ActivityLogger;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $isLoggable = ActivityLogConf::first()->user_registered;

        if ($isLoggable)
        {
            $userId = $event->user->id;
            $userName = $event->user->email;
            $action = trans('usuario creado:') . ' ' . $userName;

            $data = [
                'user_id'   => $userId,
                'user_name' => $userName,
                'action'    => $action,
            ];

            $this->logActivity($data);
        }
    }
}
