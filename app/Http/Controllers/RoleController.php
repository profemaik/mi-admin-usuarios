<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = $this->withDiffForHumans(
            Role::filter(FacadesRequest::only('search'))
                ->orderBy('name')
                ->paginate()
        );

        return Inertia::render('Roles/Index', [
            'filters' => FacadesRequest::all('search'),
            'roles' => fn () => $roles,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Roles/Create', [
            'permissionsAll' => Permission::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  => ['required', 'string', 'max:255', 'unique:roles',],
            'about' => ['nullable', 'string', 'min:15',],
            'right_permissions' => ['nullable', 'array',],
        ]);

        $selectedPermissions = [];

        foreach ($request->right_permissions as $permission)
        {
            $selectedPermissions[] = $permission['id'];
        }

        DB::transaction(function () use ($request, $selectedPermissions)
        {
            $role = new Role();
            $role->name = $request->name;
            $role->about = $request->about;
            $role->save();
            $role->permissions()->attach($selectedPermissions);
        });

        $msg = "Rol <strong>$request->name</strong> creado.";

        return Redirect::route('roles.index')->with('success', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = $this->withDiffForHumans(Role::where('id', $id)->get());

        if ($role->isEmpty())
        {
            return Redirect::back()->with('modal', false);
        }

        foreach ($role as $row)
        {
            $role = $row;
        }

        return Redirect::back()->with('modal', $role);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::with('permissions')->find($id);
        $selectedPermissions = [];

        foreach ($role->permissions as $permission)
        {
            $selectedPermissions[] = $permission['id'];
        }

        $availablePermissions = Permission::whereNotIn('id', $selectedPermissions)->get();

        return Inertia::render('Roles/Edit', [
            'role' => $role,
            'availablePermissions' => $availablePermissions,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  => [
                'required',
                'string',
                'max:255',
                Rule::unique('roles')->ignore($id),
            ],
            'about' => ['nullable', 'string', 'min:15',],
            'right_permissions' => ['nullable', 'array',],
        ]);

        DB::transaction(function () use ($request, $id)
        {
            $role = Role::find($id);
            $role->name = $request->name;
            $role->about = $request->about;
            $role->save();

            $selectedPermissions = [];

            foreach ($request->right_permissions as $permission)
            {
                $selectedPermissions[] = $permission['id'];
            }

            $role->permissions()->sync($selectedPermissions);
        });

        $msg = "Rol <strong>$request->name</strong> modificado.";

        return Redirect::route('roles.index')->with('success', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        $role->delete();

        $msg = "Rol <strong>$role->name</strong> eliminado";
        return Redirect::back()->with('success', $msg);
    }

    public function restore($id)
    {
        $role = Role::find($id);
        $role->restore();

        $msg = "Rol <strong>$role->name</strong> restaurado";

        return Redirect::back()->with('success', $msg);
    }
}
