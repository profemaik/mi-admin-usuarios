<?php

namespace App\Http\Controllers;

use App\Helpers\DateHelper;
use App\Models\Gender;
use App\Models\MaritalStatus;
use App\Models\Permission;
use App\Models\Person;
use App\Models\Role;
use App\Models\User;
use App\Models\VeCity;
use App\Models\VeMunicipality;
use App\Models\VeParish;
use App\Models\VeState;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Inertia\Inertia
     */
    public function index()
    {
        $users = User::filter(
            FacadesRequest::only(['search', 'role', 'trashed'])
        )->with('roles', 'person')->paginate(15);
        $roles = Role::all();

        return Inertia::render('Users/Index', [
            'filters' => FacadesRequest::all(['search', 'role', 'trashed']),
            'users'   => fn () => $users,
            'roles'   => $roles,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Inertia\Inertia
     */
    public function create()
    {
        return Inertia::render('Users/Create', [
            'roles'   => Role::all(),
            'genders' => Gender::all(),
            'maritalStatuses' => MaritalStatus::all(),
            'states' => VeState::all(),
            'years'  => DateHelper::yearsSinceNow(1920),
            'months' => DateHelper::monthsOfTheYear(),
            'days'   => DateHelper::daysInNumbers(),
            'permissionsAll' => Permission::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email'    => [
                'required', 'unique:users,email', 'email', 'max:255',
            ],
            'role_id'  => ['required', 'numeric', 'exists:roles,id',],
            'names'    => ['required', 'min:3', 'max:255',],
            'surnames'    => ['required', 'min:3', 'max:255',],
            'nationality' => ['required', 'min:1', 'max:1', 'regex:/[ve]/i',],
            'id_national' => [
                'required', 'unique:people,id_national', 'min:7', 'max:11',
            ],
            'birth_date'  => ['nullable', 'date',],
            'gender_id'   => ['nullable', 'numeric', 'exists:genders,id',],
            'marital_status_id'  => [
                'nullable', 'numeric', 'exists:marital_statuses,id',
            ],
            've_state_id' => ['nullable', 'numeric', 'exists:ve_states,id',],
            've_municipality_id' => [
                'nullable', 'numeric', 'exists:ve_municipalities,id',
            ],
            've_parish_id' => ['nullable', 'numeric', 'exists:ve_parishes,id',],
            've_city_id'   => ['nullable', 'numeric', 'exists:ve_cities,id',],
            'postal_code'  => ['nullable', 'string', 'min:4',],
            'address' => ['nullable', 'string', 'min:20',],
            'right_permissions' => ['nullable', 'array',],
        ]);

        $email = $request->email;
        $role = Role::find($request->role_id);
        $selectedPermissions = [];

        foreach ($request->right_permissions as $permission)
        {
            $selectedPermissions[] = $permission['id'];
        }

        DB::transaction(function () use ($email, $request, $selectedPermissions)
        {
            $user = new User();
            $user->email = $email;
            $user->password = '12345678';
            $user->is_admin = $request->role_id === '1';
            $user->save();

            $user->roles()->attach($request->role_id);
            $user->permissions()->attach($selectedPermissions);

            $person = new Person($request->only(
                'names',
                'surnames',
                'nationality',
                'id_national',
                'birth_date',
                'gender_id',
                'marital_status_id',
                've_parish_id',
                've_city_id',
                'postal_code',
                'address',
            ));
            $person->user()->associate($user);
            $person->save();

            event(new Registered($user));
        });

        $msg = "<strong>$email</strong> creado como <em>$role->name</em>.";

        return Redirect::route('users.index')->with('success', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with(
            'roles',
            'person',
            'person.gender',
            'person.maritalStatus',
            'person.veCity',
            'person.veParish.veMunicipality.veState',
        )->find($id);

        return Redirect::route('users.index')->with('modal', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with(
            'roles',
            'person',
            'person.gender',
            'person.maritalStatus',
            'person.veCity',
            'person.veParish.veMunicipality.veState',
            'permissions',
        )->find($id);

        $selectedPermissions = [];

        foreach ($user->permissions as $permission)
        {
            $selectedPermissions[] = $permission['id'];
        }

        $availablePermissions = Permission::whereNotIn('id', $selectedPermissions)->get();

        $userStateID = null;
        $userMunicipalityID = null;
        $municipalities = null;
        $parishes = null;
        $cities = null;

        if ($user->person->veCity)
        {
            $userStateID = $user->person->veCity->ve_state_id;
        }

        if ($user->person->veParish)
        {
            $userStateID = $user->person->veParish->veMunicipality->ve_state_id;
            $userMunicipalityID = $user->person->veParish->ve_municipality_id;
        }

        if ($userStateID)
        {
            $municipalities = VeMunicipality::where('ve_state_id', $userStateID)
                                            ->get();
            $cities = VeCity::where('ve_state_id', $userStateID)
                            ->get();
        }

        if ($userMunicipalityID) {
            $parishes = VeParish::where('ve_municipality_id', $userMunicipalityID)
                                ->get();
        }

        return Inertia::render('Users/Edit', [
            'user'                 => $user,
            'roles'                => Role::all(),
            'genders'              => Gender::all(),
            'maritalStatuses'      => MaritalStatus::all(),
            'states'               => VeState::all(),
            'municipalities'       => $municipalities,
            'parishes'             => $parishes,
            'cities'               => $cities,
            'years'                => DateHelper::yearsSinceNow(1920),
            'months'               => DateHelper::monthsOfTheYear(),
            'days'                 => DateHelper::daysInNumbers(),
            'availablePermissions' => $availablePermissions,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'email'       => [
                'required',
                'email',
                'max:255',
                Rule::unique('users')->ignore($id),
            ],
            'role_id'     => ['required', 'numeric', 'exists:roles,id',],
            'names'       => ['required', 'min:3', 'max:255',],
            'surnames'    => ['required', 'min:3', 'max:255',],
            'nationality' => ['required', 'min:1', 'max:1', 'regex:/[ve]/i',],
            'id_national' => [
                'required',
                Rule::unique('people')->ignore($id),
                'min:7',
                'max:11',
            ],
            'birth_date'  => ['nullable', 'date'],
            'gender_id'   => ['nullable', 'numeric', 'exists:genders,id'],
            'marital_status_id'  => [
                'nullable', 'numeric', 'exists:marital_statuses,id',
            ],
            've_state_id' => ['nullable', 'numeric', 'exists:ve_states,id',],
            've_municipality_id' => [
                'nullable', 'numeric', 'exists:ve_municipalities,id',
            ],
            've_parish_id' => ['nullable', 'numeric', 'exists:ve_parishes,id',],
            've_city_id'   => ['nullable', 'numeric', 'exists:ve_cities,id',],
            'postal_code'  => ['nullable', 'string', 'min:4',],
            'address' => ['nullable', 'string', 'min:15',],
            'right_permissions' => ['nullable', 'array',],
        ]);

        DB::transaction(function () use ($request, $id)
        {
            $user = User::find($id);
            $user->email = $request->email;
            $user->is_admin = $request->role_id === '1';
            $user->save();

            $user->roles()->sync([$request->role_id]);
            $selectedPermissions = [];

            foreach ($request->right_permissions as $permission)
            {
                $selectedPermissions[] = $permission['id'];
            }

            $user->permissions()->sync($selectedPermissions);

            $userPerson = Person::where('user_id', $id)->first();
            $userPerson->marital_status_id = $request->email;
            $userPerson->names = $request->names;
            $userPerson->surnames = $request->surnames;
            $userPerson->nationality = $request->nationality;
            $userPerson->id_national = $request->id_national;
            $userPerson->birth_date = $request->birth_date;
            $userPerson->gender_id = $request->gender_id;
            $userPerson->marital_status_id = $request->marital_status_id;
            $userPerson->ve_parish_id = $request->ve_parish_id;
            $userPerson->ve_city_id = $request->ve_city_id;
            $userPerson->postal_code = $request->postal_code;
            $userPerson->address = $request->address;
            $userPerson->save();
        });

        $msg = "<strong>$request->email</strong> modificado.";

        return Redirect::route('users.index')->with('success', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        $msg = "Usuario <strong>$user->email</strong> eliminado.";

        return Redirect::back()->with('success', $msg);
    }

    public function restore($id)
    {
        $user = User::find($id);
        $user->restore();

        $msg = "Usuario <strong>$user->email</strong> restaurado.";

        return Redirect::back()->with('success', $msg);
    }
}
