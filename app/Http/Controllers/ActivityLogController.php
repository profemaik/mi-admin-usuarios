<?php

namespace App\Http\Controllers;

use App\Models\ActivityLog;
use App\Traits\UserAgentDetails;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Inertia\Inertia;

class ActivityLogController extends Controller
{
    use UserAgentDetails;

    public function index()
    {
        $activityLog = $this->withDiffForHumans(
            ActivityLog::filter( FacadesRequest::only(['search', 'orderBy']))
                       ->latest()->paginate()
        );

        return Inertia::render('Activities/Index', [
            'filters'    => FacadesRequest::all(['search', 'orderBy']),
            'activities' => fn () => $activityLog,
        ]);
    }
}
