<?php

namespace App\Http\Controllers;

use App\Helpers\PermissionHelper;
use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Inertia\Inertia;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = $this->withDiffForHumans(
            Permission::filter(FacadesRequest::only('search'))
                      ->orderBy('route_name')
                      ->paginate()
        );

        return Inertia::render('Permissions/Index', [
            'filters' => FacadesRequest::all('search'),
            'permissions' => fn () => $permissions,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = $this->withDiffForHumans(Permission::where('id', $id)->get());

        if ($permission->isEmpty())
        {
            return Redirect::back()->with('modal', false);
        }

        foreach ($permission as $row)
        {
            $permission = $row;
        }

        return Redirect::back()->with('modal', $permission);
    }

    /**
     * Refresca los permisos según las rutas creadas.
     *
     * **Atención**: se deben definir las traducciones de las rutas en el archivo
     * `permissions.php`.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function refresh(Request $request)
    {
        $routenames = PermissionHelper::permissibleRoutes();
        $routenamesSaved = Permission::all('route_name');

        DB::transaction(function () use ($routenames, $routenamesSaved)
        {
            foreach ($routenamesSaved as $savedRoute)
            {
                if (!in_array($savedRoute->route_name, $routenames))
                {
                    Permission::where('route_name', $savedRoute->route_name)->delete();
                }
            }

            foreach ($routenames as $route)
            {
                Permission::insertOrIgnore([
                    'route_name' => $route,
                    'about'      => __("permissions.$route"),
                    'created_at' => 'now()',
                    'updated_at' => 'now()',
                ]);
            }
        });

        $msg = "Permisos actualizados.";

        return Redirect::route('permissions.index')->with('success', $msg);
    }
}
