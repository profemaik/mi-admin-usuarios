<?php

namespace App\Http\Controllers;

use App\Models\VeCity;
use App\Models\VeMunicipality;
use App\Models\VeParish;
use Illuminate\Http\Request;

class VeGeoPoliticalInfo extends Controller
{
    public function getVeMunicipalities(Request $request)
    {
        $id = $request->only('state_id');
        $municipalities = VeMunicipality::where('ve_state_id', $id)
                                        ->orderBy('name')
                                        ->get();

        return response()->json($municipalities);
    }

    public function getVeParishes(Request $request)
    {
        $id = $request->only('municipality_id');
        $parishes = VeParish::where('ve_municipality_id', $id)
                            ->orderBy('name')
                            ->get();

        return response()->json($parishes);
    }

    public function getVeCities(Request $request)
    {
        $id = $request->only('state_id');
        $cities = VeCity::where('ve_state_id', $id)
                        ->orderBy('name')
                        ->get();

        return response()->json($cities);
    }
}
