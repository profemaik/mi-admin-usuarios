<?php

namespace App\Http\Controllers;

use App\Models\MaritalStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class MaritalStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $maritalStatuses = $this->withDiffForHumans(
            MaritalStatus::filter(FacadesRequest::only('search'))
                         ->orderBy('name')
                         ->paginate()
        );

        return Inertia::render('Marital_Statuses/Index', [
            'filters' => FacadesRequest::all('search'),
            'maritalStatuses' => fn () => $maritalStatuses,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Marital_Statuses/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'    => ['required', 'string', 'max:255', 'unique:marital_statuses',],
            'about'   => ['nullable', 'string', 'min:15',],
        ]);

        DB::transaction(function () use ($request)
        {
            $maritalStatus = new MaritalStatus();
            $maritalStatus->name = $request->name;
            $maritalStatus->about = $request->about;
            $maritalStatus->save();
        });

        $msg = "Estado Civil <strong>$request->name</strong> creado.";

        return Redirect::route('marital_statuses.index')->with('success', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $maritalStatus = $this->withDiffForHumans(MaritalStatus::where('id', $id)->get());

        if ($maritalStatus->isEmpty())
        {
            return Redirect::back()->with('modal', false);
        }

        foreach ($maritalStatus as $row)
        {
            $maritalStatus = $row;
        }

        return Redirect::back()->with('modal', $maritalStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $maritalStatus = MaritalStatus::find($id);

        return Inertia::render('Marital_Statuses/Edit', [
            'gender' => $maritalStatus,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  => [
                'required',
                'string',
                'max:255',
                Rule::unique('marital_statuses')->ignore($id),
            ],
            'about' => ['nullable', 'string', 'min:15',],
        ]);

        DB::transaction(function () use ($request, $id)
        {
            $maritalStatus = MaritalStatus::find($id);
            $maritalStatus->name = $request->name;
            $maritalStatus->about = $request->about;
            $maritalStatus->save();
        });

        $msg = "Estado Civil <strong>$request->name</strong> modificado.";

        return Redirect::route('marital_statuses.index')->with('success', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $maritalStatus = MaritalStatus::find($id);
        $maritalStatus->delete();

        $msg = "Estado Civil <strong>$maritalStatus->name</strong> eliminado";
        return Redirect::back()->with('success', $msg);
    }

    public function restore($id)
    {
        $user = MaritalStatus::find($id);
        $user->restore();

        return Redirect::back()->with('success', 'Estado Civil restaurado.');
    }
}
