<?php

namespace App\Http\Controllers;

use App\Models\Gender;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class GenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $genders = $this->withDiffForHumans(
            Gender::filter(FacadesRequest::only('search'))
                  ->orderBy('name')
                  ->paginate()
        );

        return Inertia::render('Genders/Index', [
            'filters' => FacadesRequest::all('search'),
            'genders' => fn () => $genders,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Genders/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'    => ['required', 'string', 'max:255', 'unique:genders',],
            'fa_icon' => ['required', 'string', 'max:255', 'regex:/^(fa\sfa\-|fas\sfa\-|ti\-){1,1}([a-z]){2,}([a-z0-9\-])*/',],
            'about'   => ['nullable', 'string', 'min:15',],
        ]);

        DB::transaction(function () use ($request)
        {
            $gender = new Gender();
            $gender->name = $request->name;
            $gender->fa_icon = $request->fa_icon;
            $gender->about = $request->about;
            $gender->save();
        });

        $msg = "Género Social <strong>$request->name</strong> creado.";

        return Redirect::route('genders.index')->with('success', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gender = $this->withDiffForHumans(Gender::where('id', $id)->get());

        if ($gender->isEmpty())
        {
            return Redirect::back()->with('modal', false);
        }

        foreach ($gender as $row)
        {
            $gender = $row;
        }

        return Redirect::back()->with('modal', $gender);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gender = Gender::find($id);

        return Inertia::render('Genders/Edit', [
            'gender' => $gender,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'       => [
                'required',
                'string',
                'max:255',
                Rule::unique('genders')->ignore($id),
            ],
            'fa_icon' => ['required', 'string', 'max:255', 'regex:/^(fa\sfa\-|fas\sfa\-|ti\-){1,1}([a-z]){2,}([a-z0-9\-])*/',],
            'about'   => ['nullable', 'string', 'min:15',],
        ]);

        DB::transaction(function () use ($request, $id)
        {
            $gender = Gender::find($id);
            $gender->name = $request->name;
            $gender->fa_icon = $request->fa_icon;
            $gender->about = $request->about;
            $gender->save();
        });

        $msg = "Género Social <strong>$request->name</strong> modificado.";

        return Redirect::route('genders.index')->with('success', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gender = Gender::find($id);
        $gender->delete();

        $msg = "Género Social <strong>$gender->name</strong> eliminado";
        return Redirect::back()->with('success', $msg);
    }

    public function restore($id)
    {
        $user = Gender::find($id);
        $user->restore();

        return Redirect::back()->with('success', 'Género Social restaurado.');
    }
}
