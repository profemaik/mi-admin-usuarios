<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Route;

class PermissionHelper
{
    private static $excludedRoutes = [
        "ignition.healthCheck",
        "ignition.executeSolution",
        "ignition.shareReport",
        "ignition.scripts",
        "ignition.styles",
        "ignition.updateConfig",
        "ve.municipalities",
        "ve.parishes",
        "ve.cities",
        "register",
        "login",
        "password.request",
        "password.email",
        "password.reset",
        "password.update",
        "verification.notice",
        "verification.verify",
        "verification.send",
        "password.confirm",
        "logout",
    ];

    public static function permissibleRoutes()
    {
        $permissibleRoutes = [];
        $namedRoutes = collect(Route::getRoutes()->getRoutesByName())
            ->map(function ($route, $name)
            {
                if (!in_array($name, PermissionHelper::$excludedRoutes))
                {
                    return $name;
                }
            });

        foreach ($namedRoutes as $routeName)
        {
            if ($routeName !== null)
            {
                $permissibleRoutes[] = $routeName;
            }
        }

        return $permissibleRoutes;
    }
}
