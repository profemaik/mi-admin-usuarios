<?php

namespace App\Helpers;

use Carbon\Carbon;

class DateHelper
{
    public static function monthsOfTheYear()
    {
        $months = [];

        for ($m = 1; $m <= 12; $m++)
        {
            $months[] = [
                'num' => strftime('%m', mktime(0, 0, 0, $m, 1, date('Y'))),
                'str' => Carbon::create(date('Y'), $m, 1, 0, 0, 0)->monthName,
            ];
        }

        return $months;
    }

    public static function daysInNumbers()
    {
        $days = [];

        for ($m = 1; $m <= 31; $m++)
        {
            $days[] = strftime('%d', mktime(0, 0, 0, 1, $m, date('Y')));
        }

        return $days;
    }

    public static function yearsSinceNow(int $year)
    {
        return range(date('Y'), $year);
    }
}
