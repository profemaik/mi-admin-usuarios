<?php

namespace App\Traits;

use App\Models\ActivityLog;
use Illuminate\Support\Facades\Request;

/**
 * Registra las actividades del usuario en la BD del sistema.
 */
trait ActivityLogger
{
    /**
     * Registra la actividad del usuario en la BD.
     *
     * @param array $activity Datos realtivos a la acción ejecutada.
     * @return void
     */
    public function logActivity(array $activity)
    {
        $activityLog = new ActivityLog();

        $activityLog->user_id = $activity['user_id'];
        $activityLog->user_name = $activity['user_name'];
        $activityLog->action = $activity['action'];
        $activityLog->http_route = Request::fullUrl();
        $activityLog->ip_address = Request::ip();
        $activityLog->user_agent = Request::header('user-agent');
        $activityLog->locale = Request::header('accept-language');
        $activityLog->referer = Request::header('referer');
        $activityLog->method_type = Request::method();
        $activityLog->save();
    }
}
