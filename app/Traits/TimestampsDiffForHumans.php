<?php

namespace App\Traits;

use Illuminate\Support\Collection;

/**
 * Rasgo para inyectar diferencias de fechas en formato legible por humanos
 * a objetos de tipo `Illuminate\Support\Collection`.
 */
trait TimestampsDiffForHumans
{
    /**
     * Inyecta diferencias de fechas en formato legible por humanos
     * para aquellos atributos que sean fechas válidas.
     *
     * @see https://www.php.net/manual/es/function.strtotime.php
     *
     * @param  \Illuminate\Support\Collection|\Illuminate\Pagination\LengthAwarePaginator $resultSet
     * @return \Illuminate\Support\Collection
     */
    public function withDiffForHumans($resultSet): \Illuminate\Support\Collection|\Illuminate\Pagination\LengthAwarePaginator
    {
        $resultSet->map(function ($row)
        {
            foreach ($row->getAttributes() as $key => $value)
            {
                if (strtotime($value) !== false)
                {
                    $row["diff_$key"] = $row->$key->diffForHumans();
                }
            }

            return $row;
        });

        return $resultSet;
    }
}
