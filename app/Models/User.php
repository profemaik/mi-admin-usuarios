<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'email',
        'password',
        'is_admin',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = password_hash($value, PASSWORD_DEFAULT);
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search)
        {
            $query->join('people', 'users.id', '=', 'people.user_id')
                ->where(function ($query) use ($search)
                {
                    $query->where('users.email', 'ilike', '%' . $search . '%')
                        ->orWhere('people.surnames', 'ilike', '%' . $search . '%')
                        ->orWhere('people.names', 'ilike', '%' . $search . '%');
                });
        })
            ->when($filters['role'] ?? null, function ($query, $roleID)
            {
                $query->join('role_user', 'users.id', '=', 'role_user.user_id')
                    ->where('role_id', $roleID);
            })
            ->when($filters['trashed'] ?? null, function ($query, $trashed)
            {
                if ($trashed === 'with')
                {
                    $query->withTrashed();
                }
                elseif ($trashed === 'only')
                {
                    $query->onlyTrashed();
                }
            });
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }

    public function person()
    {
        return $this->hasOne(Person::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class)->withTimestamps();
    }
}
