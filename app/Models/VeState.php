<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VeState extends Model
{
    public function veMunicipalities()
    {
        return $this->hasMany(VeMunicipality::class);
    }

    public function veCities()
    {
        return $this->hasMany(VeCity::class);
    }
}
