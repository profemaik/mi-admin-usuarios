<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'email',
        'password',
        'user_id',
        'marital_status_id',
        'gender_id',
        've_parish_id',
        've_city_id',
        'nationality',
        'id_national',
        'names',
        'surnames',
        'birth_date',
        'address',
        'postal_code',
        'phones',
        'emails'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function maritalStatus()
    {
        return $this->belongsTo(MaritalStatus::class);
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class);
    }

    public function veParish()
    {
        return $this->belongsTo(VeParish::class);
    }

    public function veCity()
    {
        return $this->belongsTo(VeCity::class);
    }
}
