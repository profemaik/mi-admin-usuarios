<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'about',
    ];

    /**
     * Limita la consulta para incluir solo resultados filtrados.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $filters
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilter($query, array $filters)
    {
        return $query->when($filters['search'] ?? null, function ($query, $search)
        {
            $query->where(function ($query) use ($search)
            {
                $query->where('name', 'ilike', '%' . $search . '%')
                    ->orWhere('about', 'ilike', '%' . $search . '%');
            });
        })->when($filters['trashed'] ?? null, function ($query, $trashed)
        {
            if ($trashed === 'with')
            {
                $query->withTrashed();
            }
            elseif ($trashed === 'only')
            {
                $query->onlyTrashed();
            }
        });
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class)->withTimestamps();
    }
}
