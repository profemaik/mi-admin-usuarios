<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VeMunicipality extends Model
{
    public function veParishes()
    {
        return $this->hasMany(VeParish::class);
    }

    public function veState()
    {
        return $this->belongsTo(VeState::class);
    }
}
