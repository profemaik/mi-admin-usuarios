<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VeCity extends Model
{
    public function people()
    {
        return $this->hasMany(Person::class);
    }

    public function veState()
    {
        return $this->belongsTo(VeState::class);
    }
}
