<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ActivityLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'user_name',
        'action',
        'http_route',
        'ip_address',
        'user_agent',
        'locale',
        'referer',
        'method_type',
    ];

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null,
            fn($query, $term)
            => $query->where('user_name', 'ilike', '%'.$term.'%')
                     ->orWhere(DB::raw('ip_address::text'), 'like', '%'.$term.'%')
                     ->orWhere('http_route', 'ilike', '%'.$term.'%')
        )
        ->when($filters['orderBy'] ?? null,
            fn($query, $column)
            => $query->orderBy($column)
        );
    }
}
