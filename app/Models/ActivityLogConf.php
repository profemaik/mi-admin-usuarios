<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityLogConf extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'about',
        'auth_events_all',
        'auth_attempts',
        'auth_attempts_failed',
        'lock_out',
        'password_reset',
        'login_success',
        'logout_success',
        'logout_current_device',
        'logout_other_device',
        'user_registered',
        'user_validated',
        'user_verified',
    ];
}
