<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VeParish extends Model
{
    public function people()
    {
        return $this->hasMany(Person::class);
    }

    public function veMunicipality()
    {
        return $this->belongsTo(VeMunicipality::class);
    }
}
